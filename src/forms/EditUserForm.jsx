import React, {useState, useEffect} from 'react';

const EditUserForm = (props) => {

    useEffect(() => {
        setUser(props.currentUser)
    }, [props])

    const [user, setUser] = useState(props.currentUser);

    const handleChange = e => {
        const {name, value} = e.target;
        setUser({...user, [name]: value});
        }

    const handleSubmit = e => {
        e.preventDefault();
        if (user.name && user.author) props.updateUser(user);
    }

    return (
        <form>
            <label>Nazwa</label>
            <input className="u-full-width" type="text" value={user.name} name="name" onChange={handleChange} />
            <label>Autor</label>
            <input className="u-full-width" type="text" value={user.author} name="author" onChange={handleChange} />
            <button className="button-primary" type="submit" onClick={handleSubmit} >Edytuj</button>
            <button type="submit" onClick={() => props.setEditing(false)} >Cofnij</button>
        </form>
    )
}

export default EditUserForm;